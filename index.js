// This is licensed under GPLv3.
function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI*2);
    ctx.fillStyle = "#0087DC";
    ctx.fill();
    ctx.closePath();
}
function drawPaddle() {
    ctx.beginPath();
    ctx.rect(paddleX, canvas.height - paddleHeight, paddleWidth, paddleHeight);
    ctx.fillStyle = "#10A1CC";
    ctx.fill();
    ctx.closePath();
}
function keyDownHandler(e) {
    if (e.key == "ArrowRight" || e.key == "ArrowUp") {
        rightPressed = true;
    }
    else if (e.key == "ArrowLeft" || e.key == "ArrowDown") {
        leftPressed = true;
    }
}
function keyUpHandler(e) {
    if (e.key == "ArrowRight" || e.key == "ArrowUp") {
        rightPressed = false;
    }
    else if (e.key == "ArrowLeft" || e.key == "ArrowDown") {
        leftPressed = false;
    }
}
// Levels: green -> blue -> red.
function color(level) {
    dict = {3: "green", 2: "blue", 1: "red", 0: "rgb(120, 50, 71)"};
    return dict[level];
}
function drawBricks() {
    for(let c = 0; c < brickColumnCount; c++) {
        for(let r = 0; r < brickRowCount; r++) {
            const brickX = (c * (2 * brickRadiusX + brickPadding)) + brickOffsetLeft;
            const brickY = (r * (2 * brickRadiusY + brickPadding)) + brickOffsetTop;
            bricks[c][r].x = brickX;
            bricks[c][r].y = brickY;
            ctx.beginPath();
            ctx.ellipse(brickX, brickY, brickRadiusX, brickRadiusY, 0, 0, Math.PI*2);
            ctx.fillStyle = color(bricks[c][r].level)
            ctx.fill();
            ctx.closePath();
        }
    }
}
function collisionDetection() {
    let finished = true;
    for(let c = 0; c < brickColumnCount; c++) {
        for (let r = 0; r < brickRowCount; r++) {
            const b = bricks[c][r];
            if (b.level > 0) {
                finished = false;
                if(x > b.x - brickRadiusX && x < b.x + brickRadiusX && y > b.y - brickRadiusY && y < b.y + brickRadiusY) {
                    pop.play();
                    dy = -dy;
                    b.level -= 1;
                }
            }
        }
    }
    if(finished) {
        throw new Error("NANI?!");
    }
}
function mouseMoveHandler(e) {
    const relativeX = e.clientX - canvas.offsetLeft;
    if(relativeX > 0 && relativeX < canvas.width) {
        paddleX = relativeX - paddleWidth/2;
    }
}
function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBricks();
    drawBall();
    drawPaddle();
    drawScore();
    collisionDetection();
    if (y + dy < ballRadius) {
        dy = -dy;
    }
    else if(y + dy > canvas.height - ballRadius - paddleHeight) {
        if (x > paddleX && x < paddleX + paddleWidth) {
            dy = Math.min(dy, -dy);
        }
    }
    if (y + dy > canvas.height + ballRadius){
            alert("F");
            throw new Error("F");
    }
    if (x + dx > canvas.width - ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }

    if (rightPressed) {
    paddleX += 7;
        if (paddleX + paddleWidth > canvas.width) {
            paddleX = canvas.width - paddleWidth;
        }
    }
    else if (leftPressed) {
        paddleX -= 7;
        if (paddleX < 0) {
            paddleX = 0;
        }
    }

    x += dx;
    y += dy;
    requestAnimationFrame(draw);
}
function drawScore() {
    score = Math.max(maximumScore - performance.now(), 0);
    ctx.font = "12px FreeSans";
    ctx.fillStyle = "#DA5922";
    ctx.fillText("Score: "+score, 8, 12);
}
const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");
let x = canvas.width / 2;
let y = canvas.height - 30;
let dx = 2;
let dy = -2;
const ballRadius = 10;
const paddleHeight = 10;
const paddleWidth = 100;
let paddleX = (canvas.width - paddleWidth) / 2;
let leftPressed = false;
let rightPressed = false;
const brickRowCount = 3;
const brickColumnCount = 5;
const brickRadiusX = 60;
const brickRadiusY = 45;
const brickPadding = 10;
const brickOffsetTop = 60;
const brickOffsetLeft = 60;
const bricks = [];
let totalLevel = 0;
for(let c=0; c<brickColumnCount; c++) {
    bricks[c] = [];
    for(let r=0; r<brickRowCount; r++) {
        const level = Math.ceil(Math.random() * 3);
        totalLevel += level
        bricks[c][r] = { x: 0, y: 0, level: level };
    }
}
const music = new Audio('goldrush.ogg');
const pop = new Audio('pop.ogg');
music.play();
const maximumScore = totalLevel * 10000;
let score = maximumScore;
document.addEventListener("keydown", keyDownHandler, false)
document.addEventListener("keyup", keyUpHandler, false)
document.addEventListener("mousemove", mouseMoveHandler, false);

draw();
